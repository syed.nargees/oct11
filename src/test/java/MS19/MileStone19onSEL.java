package MS19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;



public class MileStone19onSEL {
	public static WebDriver driver;
//	public static ExtentReports extent=new ExtentReports();
//	private static ExtentSparkReporter spark=new ExtentSparkReporter("extendreport.html");
//	private static ExtentTest extentTest;
	@BeforeClass
	public void setup()
	{
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	@AfterClass
	public void setdown()
	{
		System.out.println("after the class");
	}
//	@BeforeSuite
//	public void beforesute() {
//		 extent.attachReporter(spark);
//	}
//
//	@AfterSuite
//	public void aftersuite() {
//		extent.flush();
//		  driver.close();
//	}

	@Test(priority = 1)
	public void launchApplication() {
		//extentTest=extent.createTest("launch the application");
		driver.get("https://www.demoblaze.com/index.html");

		String actualTitle = driver.getTitle();
		String expectedTitle = "STORE";
		Assert.assertEquals(actualTitle, expectedTitle);

	}

	@Test(priority = 2)
	public void loginIntoApplication() {
		//extentTest=extent.createTest("Login into  the application");
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);

		driver.findElement(By.linkText("Log in")).click();

		driver.findElement(By.id("loginusername")).sendKeys("manzoormehadi");

		driver.findElement(By.id("loginpassword")).sendKeys("Mehek@110");

		driver.findElement(By.xpath("//button[text()='Log in']")).click();

		String actualUser = driver.findElement(By.partialLinkText("Welcome manzoormehadi")).getText();
		String expectedUser = "Welcome manzoormehadi";
		Assert.assertEquals(actualUser, expectedUser);

	}

	@Test(priority = 3)
	public void addLaptopToCart() throws InterruptedException {
	//	extentTest=extent.createTest("verifyning the add to cart functionality of an application");
		driver.findElement(By.linkText("Laptops")).click();

		driver.findElement(By.linkText("Sony vaio i5")).click();

		driver.findElement(By.linkText("Add to cart")).click();

		Thread.sleep(5000);

		Alert alert = driver.switchTo().alert();

		String actualMessage = alert.getText();

		alert.accept();
		String expectedMessage = "Product added.";
		Assert.assertEquals(actualMessage, expectedMessage);

	}

	//@Test(priority = 4)
	public void purchaseProduct() throws InterruptedException {
		//extentTest=extent.createTest("PurchaseProduct");
		driver.findElement(By.linkText("Cart")).click();

		driver.findElement(By.xpath("//button[text()='Place Order']")).click();

		driver.findElement(By.id("name")).sendKeys("Manzoor");

		driver.findElement(By.id("country")).sendKeys("India");

		driver.findElement(By.id("city")).sendKeys("Bangalore");

		driver.findElement(By.id("card")).sendKeys("908890123123");

		driver.findElement(By.id("month")).sendKeys("12");

		driver.findElement(By.id("year")).sendKeys("2025");

		driver.findElement(By.xpath("//button[text()='Purchase']")).click();

		Thread.sleep(5000);

		String actualMessage = driver.findElement(By.xpath("//h2[text()='Thank you for your purchase!']")).getText();
		driver.findElement(By.xpath("//button[text()='OK']")).click();
		String expectedMessage = "Thank you for your purchase!";
		Assert.assertEquals(expectedMessage, actualMessage);

	}

	@Test(priority = 5)
	public void logoutApplication() {
		//extentTest=extent.createTest("Validating the logout functionality");
		driver.findElement(By.linkText("Log out")).click();

		String actualLgoinLink = driver.findElement(By.linkText("Log in")).getText();
		String expectedLoginLink = "Log in";
		Assert.assertEquals(expectedLoginLink, actualLgoinLink);
	}

}
